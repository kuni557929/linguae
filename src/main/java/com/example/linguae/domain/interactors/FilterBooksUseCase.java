package com.example.linguae.domain.interactors;

import com.example.linguae.domain.entity.DTO.BookDTO;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class FilterBooksUseCase implements BaseUseCase<FilterBooksUseCase.Params, List<BookDTO>> {

    private static final Logger LOG = LoggerFactory.getLogger(GetAllBooksUseCase.class);

    @Override
    public List<BookDTO> execute(Params params) {
        LOG.info("Books are being filtered with the parameter = {}", params.filter);
        return params.books
                .stream()
                .filter((book) -> params.filter.equals(Strings.EMPTY) || (book.getTitle() + " " + book.getAuthorsString()).toLowerCase().contains(params.filter.toLowerCase()))
                .collect(Collectors.toList());
    }

    @Data
    public static final class Params {

        private final List<BookDTO> books;
        private final String filter;

        public static Params getInstance(List<BookDTO> books, String filter) {
            return new Params(books, filter);
        }

    }
}
