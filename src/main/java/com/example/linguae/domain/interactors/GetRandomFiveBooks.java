package com.example.linguae.domain.interactors;

import com.example.linguae.domain.entity.DTO.BookDTO;
import com.example.linguae.domain.repository.LinguaeRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RequiredArgsConstructor
@Component
public class GetRandomFiveBooks implements BaseUseCase<GetRandomFiveBooks.Params, List<BookDTO>> {

    private static final Logger LOG = LoggerFactory.getLogger(GetRandomFiveBooks.class);

    private final LinguaeRepository linguaeRepository;

    @Override
    public List<BookDTO> execute(Params params) {
        LOG.info("Called use case to sort books with BookID = {} parameters", params.getBookId());
        Random random = new Random();
        List<BookDTO> allBooks = linguaeRepository.getBooksWithoutBookWithId(params.getBookId());
        List<BookDTO> randomFiveBooks = new ArrayList<>();

        while (randomFiveBooks.size() != 5 && allBooks.size() > 0) {
            int index = random.nextInt(0, allBooks.size());
            randomFiveBooks.add(allBooks.get(index));
            allBooks.remove(index);
        }

        return randomFiveBooks;
    }

    @Data
    public static final class Params {

        private final Integer bookId;

        public static Params getInstance(Integer bookId) {
            return new Params(bookId);
        }

    }
}
