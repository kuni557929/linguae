package com.example.linguae.domain.interactors;

import com.example.linguae.domain.entity.DTO.BookDTO;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetPrevBookByCurBookIdUserCase implements BaseUseCase<GetPrevBookByCurBookIdUserCase.Params, BookDTO> {

    @Override
    public BookDTO execute(Params params) {
        List<BookDTO> allBooks = params.getAllBooks();

        for (int i = 0; i < allBooks.size(); i++) {

            if (allBooks.get(i).getId().equals(params.getBookId())) {

                if (i > 0) {
                    return allBooks.get(i - 1);
                }

            }

        }

        return null;
    }

    @Data
    public static final class Params {

        private final Integer bookId;
        private final List<BookDTO> allBooks;

        public static Params getInstance(Integer bookId, List<BookDTO> allBooks) {
            return new Params(bookId, allBooks);
        }
    }
}
