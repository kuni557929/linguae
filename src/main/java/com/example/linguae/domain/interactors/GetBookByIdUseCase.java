package com.example.linguae.domain.interactors;

import com.example.linguae.domain.entity.DTO.BookDTO;
import com.example.linguae.domain.repository.LinguaeRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class GetBookByIdUseCase implements BaseUseCase<GetBookByIdUseCase.Params, BookDTO> {

    private static final Logger LOG = LoggerFactory.getLogger(GetBookByIdUseCase.class);

    private final LinguaeRepository linguaeRepository;

    @Override
    public BookDTO execute(Params params) {
        LOG.info("Called use case to sort books with BookID = {} parameters", params.getId());
        return linguaeRepository.getBookById(params.getId());
    }

    @Data
    public static final class Params {
        private final Integer id;

        public static Params getInstance(Integer id) {
            return new Params(id);
        }
    }
}
