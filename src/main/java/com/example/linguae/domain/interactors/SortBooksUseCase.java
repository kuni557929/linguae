package com.example.linguae.domain.interactors;

import com.example.linguae.domain.entity.DTO.BookDTO;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Component
public class SortBooksUseCase implements BaseUseCase<SortBooksUseCase.Params, List<BookDTO>> {

    private static final Logger LOG = LoggerFactory.getLogger(SortBooksUseCase.class);

    @Override
    public List<BookDTO> execute(Params params) {
        String sortField = params.sortField;
        String sortDir = params.sortDir;
        List<BookDTO> books = params.books;

        if (sortField != null && books.size() > 1) {
            LOG.info("There is sorting by field = {}, order = {}", sortField, sortDir);

            books.sort((bookFirst, bookSecond) -> {

                if (sortField.equalsIgnoreCase("languageComplexity")) {
                    return Integer.compare(bookFirst.getLanguageComplexity().getPriority(), bookSecond.getLanguageComplexity().getPriority());
                } else if (sortField.equalsIgnoreCase("createdAtYear")) {
                    return bookFirst.getCreatedAtYear().compareTo(bookSecond.getCreatedAtYear());
                } else if (sortField.equalsIgnoreCase("rating")) {
                    return bookFirst.getRating().compareTo(bookSecond.getRating());
                }

                return 0;
            });

            if (sortDir.equalsIgnoreCase("asc")) {
                Collections.reverse(books);
            }

            return books;
        }

        LOG.info("Sorting of books has not been carried out, there is a return of unsorted books");

        return books;
    }

    @Data
    public static final class Params {
        private final List<BookDTO> books;
        private final String sortField;
        private final String sortDir;

        public static Params getInstance(List<BookDTO> books, String sortField, String sortDir) {
            return new Params(books, sortField, sortDir);
        }
    }
}
