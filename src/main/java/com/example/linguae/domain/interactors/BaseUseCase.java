package com.example.linguae.domain.interactors;

public interface BaseUseCase<Params, Return> {

    Return execute(Params params);

}
