package com.example.linguae.domain.interactors;

import com.example.linguae.domain.entity.DTO.AuthorDTO;
import com.example.linguae.domain.repository.LinguaeRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class GetAuthorByIdUseCase implements BaseUseCase<GetBookByIdUseCase.Params, AuthorDTO> {

    private static final Logger LOG = LoggerFactory.getLogger(GetBookByIdUseCase.class);

    private final LinguaeRepository linguaeRepository;

    @Override
    public AuthorDTO execute(GetBookByIdUseCase.Params params) {
        LOG.info("A use case was called to get the author by id = {}", params.getId());
        return linguaeRepository.getAuthorById(params.getId());
    }

    @Data
    public static final class Params {
        private final Integer id;

        public static GetBookByIdUseCase.Params getInstance(Integer id) {
            return new GetBookByIdUseCase.Params(id);
        }
    }
}
