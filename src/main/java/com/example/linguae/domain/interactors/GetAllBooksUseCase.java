package com.example.linguae.domain.interactors;

import com.example.linguae.domain.entity.DTO.BookDTO;
import com.example.linguae.domain.repository.LinguaeRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class GetAllBooksUseCase implements BaseUseCase<Void, List<BookDTO>> {

    private static final Logger LOG = LoggerFactory.getLogger(GetAllBooksUseCase.class);

    private final LinguaeRepository repository;

    @Override
    public List<BookDTO> execute(Void unused) {
        LOG.info("Called use case to get all books");
        return repository.getAllBooks();
    }
}
