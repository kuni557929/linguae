package com.example.linguae.domain.repository;

import com.example.linguae.domain.entity.DTO.AuthorDTO;
import com.example.linguae.domain.entity.DTO.BookDTO;

import java.util.List;

public interface LinguaeRepository {

    /**
     * Получить все книги.
     *
     * @return список книг.
     */
    List<BookDTO> getAllBooks();

    /**
     * Получить книгу по id.
     *
     * @param id - id книги.
     * @return найденную книгу по id.
     */
    BookDTO getBookById(Integer id);

    /**
     * Получить автора по id.
     *
     * @param id автора, по которому будем искать.
     * @return найденного автора по id.
     */
    AuthorDTO getAuthorById(Integer id);

    /**
     * Получить все книги без книги с id, равным переданному параметру {@param bookId}
     *
     * @param bookId - id книги
     * @return список книг
     */
    List<BookDTO> getBooksWithoutBookWithId(Integer bookId);
}
