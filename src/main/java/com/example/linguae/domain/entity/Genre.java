package com.example.linguae.domain.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Genre {
    CHILDREN_LITERATURE("Детская литература"),
    DETECTIVE("Детектив"),
    HUMOR("Юмор"),
    SATIRE("Сатира"),
    MELODRAMA("Мелодрама"),
    FANTASY("Фантастика"),
    DRAMA("Драма"),
    ADVENTURES("Приключения");

    private final String genre;

    public String getValue() {
        return genre;
    }
}
