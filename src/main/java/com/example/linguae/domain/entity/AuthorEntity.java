package com.example.linguae.domain.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthorEntity {
    private final Integer id;
    private final String name;
    private final String description;
    private final String imageUrl;
}
