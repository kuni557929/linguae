package com.example.linguae.domain.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Language {
    ENGLISH("Английский");

    private final String language;

    public String getValue() {
        return language;
    }
}
