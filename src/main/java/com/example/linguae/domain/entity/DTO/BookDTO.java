package com.example.linguae.domain.entity.DTO;

import com.example.linguae.domain.entity.Genre;
import com.example.linguae.domain.entity.Language;
import com.example.linguae.domain.entity.LanguageComplexity;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@Data
public class BookDTO {
    private final Integer id;
    private final String title;
    private final String description;
    private final List<AuthorDTO> authors;
    private final List<Genre> genres;
    private final Integer createdAtYear;
    private final LanguageComplexity languageComplexity;
    private final Integer numberOfPages;
    private final String imageUrl;
    private final Integer rating;
    private final Language language;
    private final String contentPage;
    private final String filmUrl;
    private List<BookDTO> similarBooks;

    public String getGenresString() {
        return genres
                .stream()
                .map(Genre::getValue)
                .collect(Collectors.joining(", "));
    }

    public String getAuthorsString() {
        return getAuthors()
                .stream()
                .map(AuthorDTO::getName)
                .collect(Collectors.joining(", "));
    }
}
