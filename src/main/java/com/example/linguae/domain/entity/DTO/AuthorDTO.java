package com.example.linguae.domain.entity.DTO;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AuthorDTO {
    private final Integer id;
    private final String name;
    private final String description;
    private final String imageUrl;
    private List<BookDTO> books;
}
