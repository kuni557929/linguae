package com.example.linguae.domain.entity;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class BookEntity {
    private final Integer id;
    private final String title;
    private final String description;
    private final List<Integer> authorsIds;
    private final List<Genre> genres;
    private final Integer createdAtYear;
    private final LanguageComplexity languageComplexity;
    private final Integer numberOfPages;
    private final String imageUrl;
    private final Integer rating;
    private final Language language;
    private final String contentPage;
    private final String filmUrl;

    public String getGenre() {
        StringBuilder authors = new StringBuilder();

        for (Genre genre : this.genres) {
            authors.append(genre.getValue()).append(", ");
        }

        return authors.substring(0, authors.length() - 2);
    }
}
