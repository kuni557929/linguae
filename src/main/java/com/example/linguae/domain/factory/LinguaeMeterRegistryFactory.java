package com.example.linguae.domain.factory;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class LinguaeMeterRegistryFactory {

    private final MeterRegistry meterRegistry;

    public Counter createCounter(String name) {
        return meterRegistry.counter(name);
    }

}
