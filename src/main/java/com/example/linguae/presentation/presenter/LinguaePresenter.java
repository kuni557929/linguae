package com.example.linguae.presentation.presenter;

import com.example.linguae.domain.entity.DTO.AuthorDTO;
import com.example.linguae.domain.entity.DTO.BookDTO;
import com.example.linguae.domain.interactors.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import java.util.List;

@RequiredArgsConstructor
@Component
public class LinguaePresenter {

    private static final Logger LOG = LoggerFactory.getLogger(LinguaePresenter.class);

    public static final String BOOKS_ATTRIBUTE = "books";
    public static final String SORT_DIR_ATTRIBUTE = "sortDir";
    public static final String SORT_FIELD_ATTRIBUTE = "sortField";
    public static final String FILTER_ATTRIBUTE = "filter";
    public static final String BOOK_ATTRIBUTE = "book";
    public static final String AUTHOR_ATTRIBUTE = "author";
    public static final String IS_LAST_ATTRIBUTE = "isLast";
    public static final String IS_FIRST_ATTRIBUTE = "isFirst";
    public static final String PREV_BOOK_ID_ATTRIBUTE = "prevBookId";
    public static final String NEXT_BOOK_ID_ATTRIBUTE = "nextBookId";

    private final SortBooksUseCase sortBooksUseCase;
    private final GetAllBooksUseCase getAllBooksUseCase;
    private final FilterBooksUseCase filterBooksUseCase;
    private final GetBookByIdUseCase getBookByIdUseCase;
    private final GetAuthorByIdUseCase getAuthorByIdUseCase;
    private final GetRandomFiveBooks getRandomFiveBooks;
    private final GetPrevBookByCurBookIdUserCase getPrevBookByCurBookIdUserCase;
    private final GetNextBookByCurBookIdUserCase getNextBookByCurBookIdUserCase;

    public void getAllBooks(String sortField,
                            String sortDir,
                            String filter,
                            Model model) {
        LOG.info("The process of getting all the books is underway");
        List<BookDTO> allBooks = getAllBooksUseCase.execute(null);
        LOG.info("All books received: {}", allBooks);

        LOG.info("There is a process of filtering books");
        List<BookDTO> filteredBooks = filterBooksUseCase.execute(FilterBooksUseCase.Params.getInstance(allBooks, filter));
        LOG.info("Books are filtered out: {}", filteredBooks);

        LOG.info("ID process sorting books by parameter sortField = {}, sortdir = {}", sortField, sortDir);
        List<BookDTO> sortedBooks = sortBooksUseCase.execute(SortBooksUseCase.Params.getInstance(filteredBooks, sortField, sortDir));
        LOG.info("Books are sorted: {}", sortedBooks);

        LOG.info("Adding the attribute: {}", BOOKS_ATTRIBUTE);
        model.addAttribute(BOOKS_ATTRIBUTE, sortedBooks);

        LOG.info("Adding the sortDir attribute: {}", sortDir);
        model.addAttribute(SORT_DIR_ATTRIBUTE, sortDir);

        LOG.info("Adding the SortField attribute: {}", sortField);
        model.addAttribute(SORT_FIELD_ATTRIBUTE, sortField);

        LOG.info("Adding the filter attribute: {}", filter);
        model.addAttribute(FILTER_ATTRIBUTE, filter);
    }

    public void getBookById(Integer id, Model model) {
        LOG.info("The process of getting a book by id = {} is underway", id);
        BookDTO foundBook = getBookByIdUseCase.execute(GetBookByIdUseCase.Params.getInstance(id));
        LOG.info("The book was found: {}", foundBook);

        LOG.info("The process of getting a similar books is underway");
        List<BookDTO> similarBooks = getRandomFiveBooks.execute(GetRandomFiveBooks.Params.getInstance(foundBook.getId()));
        LOG.info("Similar books was found: {}", similarBooks);

        foundBook.setSimilarBooks(similarBooks);

        LOG.info("Setting the book attribute");
        model.addAttribute(BOOK_ATTRIBUTE, foundBook);
    }

    public void getAuthorById(Integer id, Model model) {
        LOG.info("The process of obtaining the author by id is underway");
        AuthorDTO authorDTO = getAuthorByIdUseCase.execute(GetAuthorByIdUseCase.Params.getInstance(id));
        LOG.info("The author was found: {}", authorDTO);

        LOG.info("The process of getting a similar books is underway");
        List<BookDTO> similarBooks = getRandomFiveBooks.execute(GetRandomFiveBooks.Params.getInstance(-1));
        LOG.info("Similar books was found: {}", similarBooks);

        authorDTO.setBooks(similarBooks);

        LOG.info("Setting the author attribute");
        model.addAttribute(AUTHOR_ATTRIBUTE, authorDTO);
    }

    public String getContentBook(Integer id, Model model) {
        LOG.info("The process of obtaining the book by id is underway");
        BookDTO bookDTO = getBookByIdUseCase.execute(GetBookByIdUseCase.Params.getInstance(id));
        LOG.info("The book was found: {}", bookDTO);

        LOG.info("Setting the author attribute");
        model.addAttribute(BOOK_ATTRIBUTE, bookDTO);

        return bookDTO.getContentPage();
    }

    public void getPrevAndNextBookByCurBookId(Integer curBookId, Model model) {
        LOG.info("The process of getting all the books is underway");
        List<BookDTO> allBooks = getAllBooksUseCase.execute(null);

        LOG.info("The process of getting a book by id = {} is underway", curBookId);
        BookDTO foundBook = getBookByIdUseCase.execute(GetBookByIdUseCase.Params.getInstance(curBookId));

        LOG.info("The process of getting prev book");
        BookDTO prevBook = getPrevBookByCurBookIdUserCase.execute(GetPrevBookByCurBookIdUserCase.Params.getInstance(curBookId, allBooks));

        LOG.info("The process of getting next book");
        BookDTO nextBook = getNextBookByCurBookIdUserCase.execute(GetNextBookByCurBookIdUserCase.Params.getInstance(curBookId, allBooks));

        LOG.info("Adding the attribute");
        model.addAttribute(BOOK_ATTRIBUTE, foundBook);
        model.addAttribute(IS_FIRST_ATTRIBUTE, prevBook == null);
        model.addAttribute(IS_LAST_ATTRIBUTE, nextBook == null);
        model.addAttribute(PREV_BOOK_ID_ATTRIBUTE, prevBook == null ? -1 : prevBook.getId());
        model.addAttribute(NEXT_BOOK_ID_ATTRIBUTE, nextBook == null ? -1 : nextBook.getId());
    }
}
