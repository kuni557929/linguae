package com.example.linguae.presentation.view;

import com.example.linguae.domain.factory.LinguaeMeterRegistryFactory;
import com.example.linguae.presentation.presenter.LinguaePresenter;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Timed
@Controller
public class LinguaeController {

    public static final String MAIN_PAGE_URL = "/";
    public static final String BOOKS_PAGE_URL = "/books";
    public static final String GET_BOOK_URL = "/book";
    public static final String GET_AUTHOR_URL = "/author";
    public static final String GET_CONTENT_BOOK_URL = "/book/content";
    public static final String GET_USER_AGREEMENTS_URL = "/userAgreements";
    public static final String GET_PREMIUM_URL = "/premiumPage";
    public static final String GET_FLOW_FILMS_URL = "/flowFilms";
    public static final String GET_SCRABBLE_URL = "/scrabble";

    private final LinguaePresenter linguaePresenter;
    private final Counter numberOfGetAllBooksRequests;
    private final Counter numberOfGetBookRequests;
    private final Counter numberOfGetAuthorRequests;
    private final Counter numberOfGetContentBook;
    private final Counter numberOfGetMainPage;
    private final Counter numberOfGetUserAgreementsPage;
    private final Counter numberOfGetPremiumPage;
    private final Counter numberOfGetFlowFilmsPage;
    private final Counter numberOfGetScrabblePage;

    @Autowired
    public LinguaeController(LinguaePresenter linguaePresenter,
                             LinguaeMeterRegistryFactory meterRegistryFactory) {
        this.linguaePresenter = linguaePresenter;
        this.numberOfGetAllBooksRequests = meterRegistryFactory.createCounter("number_of_get_all_books_requests");
        this.numberOfGetBookRequests = meterRegistryFactory.createCounter("number_of_get_book_requests");
        this.numberOfGetAuthorRequests = meterRegistryFactory.createCounter("number_of_get_author_requests");
        this.numberOfGetContentBook =meterRegistryFactory.createCounter("number_of_get_content_book_requests");
        this.numberOfGetMainPage = meterRegistryFactory.createCounter("number_of_get_main_page_requests");
        this.numberOfGetUserAgreementsPage = meterRegistryFactory.createCounter("number_of_get_user_agreements_page");
        this.numberOfGetPremiumPage = meterRegistryFactory.createCounter("number_of_get_premium_requests");
        this.numberOfGetFlowFilmsPage = meterRegistryFactory.createCounter("number_of_get_flow_films_requests");
        this.numberOfGetScrabblePage = meterRegistryFactory.createCounter("number_of_get_scrabble_page_requests");
    }

    @GetMapping(MAIN_PAGE_URL)
    public String mainPage() {
        numberOfGetMainPage.increment();
        return "MainPage";
    }

    @GetMapping(GET_USER_AGREEMENTS_URL)
    public String getUserAgreements() {
        numberOfGetUserAgreementsPage.increment();
        return "UserAgreementsPage";
    }

    @GetMapping(GET_PREMIUM_URL)
    public String getPremiumPage() {
        numberOfGetPremiumPage.increment();
        return "PremiumPage";
    }

    @GetMapping(GET_SCRABBLE_URL)
    public String getScrabblePage() {
        numberOfGetScrabblePage.increment();
        return "ScrabblePage";
    }

    @GetMapping(BOOKS_PAGE_URL)
    public String getAllBooks(@RequestParam(name = "sort-field", defaultValue = Strings.EMPTY) String sortField,
                              @RequestParam(name = "filter", defaultValue = Strings.EMPTY) String filter,
                              @RequestParam(name = "sort-dir", defaultValue = "asc") String sortDir,
                              Model model) {
        numberOfGetAllBooksRequests.increment();
        linguaePresenter.getAllBooks(sortField, sortDir, filter, model);
        return "BooksPage";
    }

    @GetMapping(GET_BOOK_URL)
    public String getBook(@RequestParam(name = "id") Integer id,
                          Model model) {
        numberOfGetBookRequests.increment();
        linguaePresenter.getBookById(id, model);
        return "BookInfoPage";
    }

    @GetMapping(GET_AUTHOR_URL)
    public String getAuthor(@RequestParam(name = "id") Integer id,
                            Model model) {
        numberOfGetAuthorRequests.increment();
        linguaePresenter.getAuthorById(id, model);
        return "AuthorPage";
    }

    @GetMapping(GET_CONTENT_BOOK_URL)
    public String getContentBook(@RequestParam(name = "id") Integer id,
                                 Model model) {
        numberOfGetContentBook.increment();
        return "contentOfBooks/" + linguaePresenter.getContentBook(id, model);
    }

    @GetMapping(GET_FLOW_FILMS_URL)
    public String getFlowFilms(@RequestParam(name = "id") Integer id,
                               Model model) {
        numberOfGetFlowFilmsPage.increment();
        linguaePresenter.getPrevAndNextBookByCurBookId(id, model);
        return "FilmsPage";
    }
}
