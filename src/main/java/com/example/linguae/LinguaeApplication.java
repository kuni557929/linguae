package com.example.linguae;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LinguaeApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinguaeApplication.class, args);
	}
}
