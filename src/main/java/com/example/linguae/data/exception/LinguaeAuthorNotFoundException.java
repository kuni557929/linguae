package com.example.linguae.data.exception;

public class LinguaeAuthorNotFoundException extends RuntimeException {

    public LinguaeAuthorNotFoundException(String message) {
        super(message);
    }

}
