package com.example.linguae.data.exception;

public class LinguaeBookNotFoundException extends RuntimeException {

    public LinguaeBookNotFoundException(String message) {
        super(message);
    }

}
