package com.example.linguae.data.repository.datastore;

import com.example.linguae.data.exception.LinguaeAuthorNotFoundException;
import com.example.linguae.data.exception.LinguaeBookNotFoundException;
import com.example.linguae.data.mapper.LinguaeAuthorMapper;
import com.example.linguae.data.mapper.LinguaeBookMapper;
import com.example.linguae.data.repository.database.LinguaeDataBaseImpl;
import com.example.linguae.domain.entity.BookEntity;
import com.example.linguae.domain.entity.DTO.AuthorDTO;
import com.example.linguae.domain.entity.DTO.BookDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class LinguaeDataStoreDataBase implements LinguaeDataStore {

    private final LinguaeDataBaseImpl linguaeDataBase;
    private final LinguaeBookMapper linguaeBookMapper;
    private final LinguaeAuthorMapper linguaeAuthorMapper;

    @Override
    public List<BookDTO> getAllBooks() {
        List<BookDTO> bookDTOs = new ArrayList<>();

        for (BookEntity book : linguaeDataBase.getAllBooks()) {
            BookDTO bookDTO = linguaeBookMapper.toDTO(book);
            bookDTO.getAuthors().addAll(book.getAuthorsIds().stream().map(this::getAuthorById).toList());
            bookDTOs.add(bookDTO);
        }

        return bookDTOs;
    }

    @Override
    public BookDTO getBookById(Integer id) {
        BookEntity bookEntity = linguaeDataBase.getBookById(id).orElseThrow(() -> new LinguaeBookNotFoundException(String.format("Book with id = %d not found!", id)));
        BookDTO bookDTO = linguaeBookMapper.toDTO(bookEntity);
        bookDTO.getAuthors().addAll(bookEntity.getAuthorsIds().stream().map(this::getAuthorById).toList());
        return bookDTO;
    }

    @Override
    public AuthorDTO getAuthorById(Integer id) {
        return linguaeAuthorMapper.toDTO(linguaeDataBase.getAuthorById(id).orElseThrow(() -> new LinguaeAuthorNotFoundException(String.format("Author with id = %d not found!", id))));
    }

    @Override
    public List<BookDTO> getBooksWithoutBookWithId(Integer id) {
        return getAllBooks()
                .stream()
                .filter(book -> !book.getId().equals(id))
                .collect(Collectors.toList());
    }
}
