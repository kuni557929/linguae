package com.example.linguae.data.repository;

import com.example.linguae.data.repository.datastore.LinguaeDataStoreCloud;
import com.example.linguae.data.repository.datastore.LinguaeDataStoreDataBase;
import com.example.linguae.domain.entity.DTO.AuthorDTO;
import com.example.linguae.domain.entity.DTO.BookDTO;
import com.example.linguae.domain.repository.LinguaeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@RequiredArgsConstructor
@Repository
public class LinguaeDataRepository implements LinguaeRepository {

    private final LinguaeDataStoreDataBase linguaeDataStoreDataBase;
    private final LinguaeDataStoreCloud linguaeDataStoreCloud;

    @Override
    public List<BookDTO> getAllBooks() {
        return linguaeDataStoreDataBase.getAllBooks();
    }

    @Override
    public BookDTO getBookById(Integer id) {
        return linguaeDataStoreDataBase.getBookById(id);
    }

    @Override
    public AuthorDTO getAuthorById(Integer id) {
        return linguaeDataStoreDataBase.getAuthorById(id);
    }

    @Override
    public List<BookDTO> getBooksWithoutBookWithId(Integer bookId) {
        return linguaeDataStoreDataBase.getBooksWithoutBookWithId(bookId);
    }
}
