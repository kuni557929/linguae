package com.example.linguae.data.repository.datastore;

import com.example.linguae.data.exception.LinguaeOperationNotSupportedException;
import com.example.linguae.domain.entity.DTO.AuthorDTO;
import com.example.linguae.domain.entity.DTO.BookDTO;

import java.util.List;

public interface LinguaeDataStore {

    /**
     * Получить все книги.
     *
     * @return список книг.
     */
    default List<BookDTO> getAllBooks() {
        throw new LinguaeOperationNotSupportedException();
    }

    /**
     * Получить книгу по id.
     *
     * @param id книги.
     * @return найденную книгу по id.
     */
    default BookDTO getBookById(Integer id) {
        throw new LinguaeOperationNotSupportedException();
    }

    /**
     * Получить автора по id.
     *
     * @param id автора, по которому будем искать.
     * @return найденного автора по id.
     */
    default AuthorDTO getAuthorById(Integer id) {
        throw new LinguaeOperationNotSupportedException();
    }

    /**
     * Получить все книги без книги с id, равным переданному параметру {@param id}
     *
     * @param id - id книги
     * @return список книг
     */
    default List<BookDTO> getBooksWithoutBookWithId(Integer id) {
        throw new LinguaeOperationNotSupportedException();
    }
}
