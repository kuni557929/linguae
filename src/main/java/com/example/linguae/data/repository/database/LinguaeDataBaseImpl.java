package com.example.linguae.data.repository.database;

import com.example.linguae.data.repository.database.DAO.LinguaeAuthorDAO;
import com.example.linguae.data.repository.database.DAO.LinguaeBookDAO;
import com.example.linguae.domain.entity.AuthorEntity;
import com.example.linguae.domain.entity.BookEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Component
public class LinguaeDataBaseImpl implements LinguaeDataBase {

    private final LinguaeBookDAO linguaeBookDAO;
    private final LinguaeAuthorDAO linguaeAuthorDAO;

    @Override
    public List<BookEntity> getAllBooks() {
        return linguaeBookDAO.getBooks();
    }

    @Override
    public Optional<BookEntity> getBookById(Integer id) {
        return linguaeBookDAO
                .getBooks()
                .stream()
                .filter(book -> book.getId().equals(id))
                .findFirst();
    }

    public Optional<AuthorEntity> getAuthorById(Integer id) {
        return linguaeAuthorDAO
                .getAuthors()
                .stream()
                .filter(author -> author.getId().equals(id))
                .findFirst();
    }
}
