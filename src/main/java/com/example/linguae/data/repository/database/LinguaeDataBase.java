package com.example.linguae.data.repository.database;

import com.example.linguae.domain.entity.BookEntity;

import java.util.List;
import java.util.Optional;

public interface LinguaeDataBase {

    /**
     * Получить все книги.
     *
     * @return список всех книг.
     */
    List<BookEntity> getAllBooks();

    /**
     * Получить книгу по id.
     *
     * @param id - id книги.
     * @return найденную книгу по id.
     */
    Optional<BookEntity> getBookById(Integer id);
}
