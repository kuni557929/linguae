package com.example.linguae.data.mapper;

import com.example.linguae.domain.entity.BookEntity;
import com.example.linguae.domain.entity.DTO.BookDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class LinguaeBookMapper {

    public BookDTO toDTO(BookEntity entity) {
        return BookDTO.builder()
                .id(entity.getId())
                .title(entity.getTitle())
                .description(entity.getDescription())
                .authors(new ArrayList<>())
                .genres(entity.getGenres())
                .createdAtYear(entity.getCreatedAtYear())
                .languageComplexity(entity.getLanguageComplexity())
                .numberOfPages(entity.getNumberOfPages())
                .imageUrl(entity.getImageUrl())
                .rating(entity.getRating())
                .language(entity.getLanguage())
                .contentPage(entity.getContentPage())
                .filmUrl(entity.getFilmUrl())
                .build();
    }

}
