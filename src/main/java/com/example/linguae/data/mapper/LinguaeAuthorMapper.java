package com.example.linguae.data.mapper;

import com.example.linguae.domain.entity.AuthorEntity;
import com.example.linguae.domain.entity.DTO.AuthorDTO;
import org.springframework.stereotype.Component;

@Component
public class LinguaeAuthorMapper {

    public AuthorDTO toDTO(AuthorEntity author) {
        return AuthorDTO.builder()
                .id(author.getId())
                .description(author.getDescription())
                .imageUrl(author.getImageUrl())
                .name(author.getName())
                .build();
    }

}
