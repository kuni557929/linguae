document.addEventListener( 'DOMContentLoaded', function( event ) {
    const img = document.getElementById("img");
    img.addEventListener('click', () => {
        if (img.alt === 'loon') {
            img.src = "/images/header/sun.svg";
            img.alt = "sun";
            document.getElementById("header-container").classList.add("header-black-theme");
            document.getElementById("footer-container").classList.add("footer-black-theme");
            document.getElementById("content").classList.add("container-black-theme");
        } else {
            img.src = "/images/header/loon.svg";
            img.alt = "loon";
            document.getElementById("header-container").classList.remove("header-black-theme");
            document.getElementById("footer-container").classList.remove("footer-black-theme");
            document.getElementById("content").classList.remove("container-black-theme");
        }
    });
});